from sqlalchemy.orm import Session
from ..entity.user import User 


def get_user(db: Session, user_id: int):
    return db.query(User).filter(User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(User).filter(User.email == email and User.is_active==True).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(User).offset(skip).limit(limit).all()


def create_user(db: Session, db_user:User):
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user