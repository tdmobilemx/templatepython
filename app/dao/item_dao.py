from sqlalchemy.orm import Session
from app.entity.item import Item

def create_user_item(db: Session, db_item: Item):
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

def get_items(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Item).offset(skip).limit(limit).all()