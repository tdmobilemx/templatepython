from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from .entity import user, item, role
from app.routers import user_router, item_router, extra_router

from .conf.database import engine

user.Base.metadata.create_all(bind=engine)
role.Base.metadata.create_all(bind=engine)
item.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)


@app.get("/", tags=["root"])
async def read_root() -> dict:
    engine.execute("INSERT INTO roles (id, role, description) VALUES (1,'ROLE_ADMIN','Administrador');")
    return {"message": "Welcome"}

app.include_router(user_router.router, prefix="/users", tags=["users"])
app.include_router(item_router.router, prefix="/items", tags=["items"])
"""Servicios extra para procesos largos o consumo de repositorios"""
app.include_router(extra_router.router, prefix="/extra", tags=["extras"])
