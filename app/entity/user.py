from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from ..conf.database import Base

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    fullname = Column(String)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)
    #role_id = Column(Integer, ForeignKey("roles.id"))
    role_id = Column(Integer)
    
    #role = relationship("Role", back_populates="users")
    items = relationship("Item", back_populates="owner")
