import time
import logging
import httpx
import json
from types import SimpleNamespace
from typing import List
from fastapi import APIRouter,HTTPException
from starlette.background import BackgroundTasks

router = APIRouter()

def taskLarge():
    time.sleep(10)
    logging.debug("Esperaste 10 segundos")
    


@router.get(path="/taskBackground", response_description="Informacion")
async def root(background_tasks: BackgroundTasks):
    background_tasks.add_task(taskLarge)
    return {"message": "HelloWorld"}


@router.get("/uuid")
async def main():
    headers = {
        'accept': 'application/json',
        'cache-control': 'no-cache',
        'content-type': 'application/json'
    }
    response = httpx.get(
        "https://jsonplaceholder.typicode.com/users", headers=headers)
    if response.status_code == httpx.codes.OK:
        data: List = json.loads(
            response.text, object_hook=lambda d: SimpleNamespace(**d))
        """for inf in data:
            print(inf.id)"""
        return response.json()
    else:
        raise HTTPException(status_code=400, detail=response.text)
