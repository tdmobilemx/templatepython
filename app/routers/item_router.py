from typing import List
from app.conf.database import SessionLocal
from fastapi import APIRouter
from fastapi.params import Body, Depends
from app.schema import item_schema
from app.dependencies import get_db
from app.service import item_service
from app.auth.auth_bearer import JWTBearer

router = APIRouter()

@router.post("/", response_model=item_schema.ItemSchema, dependencies=[Depends(JWTBearer())])
async def create_item_user(item: item_schema.ItemCreateSchema = Body(...), db: SessionLocal = Depends(get_db)) -> dict:
    return item_service.create_item(db=db, item=item)


@router.get("/", response_model=List[item_schema.ItemSchema], dependencies=[Depends(JWTBearer())])
async def get_all_items(db: SessionLocal = Depends(get_db)) -> dict:
    return item_service.get_all(db=db)
