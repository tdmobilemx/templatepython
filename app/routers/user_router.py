from typing import List
from app.conf.database import SessionLocal
from fastapi import APIRouter, status
from fastapi.params import Body, Depends
from app.schema import user_schema
from app.dependencies import get_db
from app.service import user_service
from app.auth.auth_bearer import JWTBearer

router = APIRouter()

@router.post("/singup", response_model=user_schema.UserItemSchema, status_code=status.HTTP_201_CREATED)
async def create_user(user: user_schema.UserCreateSchema = Body(...), db: SessionLocal = Depends(get_db)):
    return user_service.create_user(db=db, user=user)


@router.post("/signin", response_model=user_schema.UserTokenSchema, status_code=status.HTTP_200_OK)
async def signup_user(user: user_schema.UserLoginSchema = Body(...), db: SessionLocal = Depends(get_db)):
    return user_service.singUp(db=db, user=user)


@router.get("/", response_model=List[user_schema.UserItemSchema],dependencies=[Depends(JWTBearer(roles_allow=['ROLE_ADMIN']))])
async def read_all_user(db: SessionLocal = Depends(get_db)) -> dict:
    return user_service.getAll(db=db)


@router.get("/{id}", response_model=user_schema.UserItemSchema,dependencies=[Depends(JWTBearer(roles_allow=['ROLE_ADMIN']))] )
async def read_id_user(id: int, db: SessionLocal = Depends(get_db)) -> dict:
    return user_service.getById(id=id, db=db)