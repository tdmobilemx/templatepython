from typing import List
from app.auth.auth_handler import signJWT
import bcrypt
from fastapi.exceptions import HTTPException
from app.schema import user_schema
from app.conf.database import SessionLocal
from app.dao import user_dao
from app.entity.user import User


def create_user(user: user_schema.UserCreateSchema, db: SessionLocal):
    db_user = user_dao.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")

    salt = bcrypt.gensalt()
    hash_pass = bcrypt.hashpw(user.password.encode(), salt)
    userEntity = User(email = user.email, 
                      fullname = user.fullname,
                      hashed_password = hash_pass, 
                      is_active = True,
                      role_id = 1)

    user_create: user.User = user_dao.create_user(db, db_user=userEntity)
    return user_schema.UserItemSchema(
        id=user_create.id,
        fullname=user_create.fullname,
        email=user_create.email
    )


def singUp(user: user_schema.UserLoginSchema, db: SessionLocal):
    db_user: User = user_dao.get_user_by_email(db, email=user.email)
    if not db_user:
        if not (bcrypt.checkpw(user.password.encode(), db_user.hashed_password)):
            raise HTTPException(status_code=404, detail="Not found")
    else:
        return user_schema.UserTokenSchema(
            id=db_user.id,
            email=db_user.email,
            fullname=db_user.fullname,
            access_token=signJWT(user.email)
        )


def getById(id: int, db: SessionLocal):
    db_user: User = user_dao.get_user(db, user_id=id)
    return user_schema.UserItemSchema(
        id=db_user.id,
        fullname=db_user.fullname,
        email=db_user.email)


def getAll(db: SessionLocal):
    db_users: List[User] = user_dao.get_users(db)
    user_lst = list(map(lambda x: user_schema.UserItemSchema(
        id=x.id,
        fullname=x.fullname,
        email=x.email), db_users))
    return user_lst
