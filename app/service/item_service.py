from app.conf.database import SessionLocal
from app.dao import item_dao
from app.schema.item_schema import ItemCreateSchema,ItemSchema
from app.entity.item import Item

def create_item(item: ItemCreateSchema, db: SessionLocal):
    db_item = Item(**item.dict())
    item_create:Item=item_dao.create_user_item(db=db,db_item=db_item)
    return ItemSchema(**item_create.__dict__)

def get_all(db: SessionLocal):
    items:Item=item_dao.get_items(db)
    item_lst=list(map(lambda x: ItemSchema(**x.__dict__), items))
    return item_lst