
import time
from typing import Dict, List

import jwt
from decouple import config

JWT_SECRET = config("secret")
JWT_ALGORITHM = config("algorithm")

def signJWT(user_id: str): #-> Dict[str, str]:
    payload = {
        "sub": user_id,
        "iat": time.time(),
        "exp": time.time() + 3600,
        "roles":["ROLE_ADMIN"]
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

    return token


def decodeJWT(token: str): # -> dict:
    try:
        decoded_token = jwt.decode(
            token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decoded_token
    except:
        return {}
