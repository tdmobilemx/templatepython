import time
from typing import List
from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from .auth_handler import decodeJWT


class JWTBearer(HTTPBearer):
    roles_allow: List[str]

    def __init__(self, auto_error: bool = True, roles_allow: List[str] = []):
        super(JWTBearer, self).__init__(auto_error=auto_error)
        self.roles_allow = roles_allow

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(
                    status_code=403, detail="Invalid authentication scheme.")
            if not self.verify_jwt(credentials.credentials):
                raise HTTPException(
                    status_code=403, detail="Invalid token or expired token.")
            return credentials.credentials
        else:
            raise HTTPException(
                status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, jwtoken: str):
        isTokenValid: bool = False
        try:
            payload = decodeJWT(jwtoken)
            isTokenValid = self.validate_payload(payload)
        except:
            payload = None
        return isTokenValid

    def validate_payload(self, decoded_token: dict):
        validate_exp = False
        # Se valida si se solicita rol especifico
        validate_roles = (True if len(self.roles_allow) == 0 else False)
        if decoded_token["exp"] >= time.time():
            validate_exp = True
            for role in decoded_token["roles"]:
                for role_allow in self.roles_allow:
                    if role == role_allow:
                        validate_roles = True
                        break
        return validate_exp and validate_roles
