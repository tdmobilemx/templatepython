from pydantic import BaseModel, Field, EmailStr

class UserCreateSchema(BaseModel):
    fullname: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "fullname": "Lorem ipsum dolor sit",
                "email": "lorem@ipsum.com",
                "password": "weakpassword"
            }
        }
        
class UserItemSchema(BaseModel):
    id: int = Field(...)
    fullname: str = Field(...)
    email: EmailStr = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "fullname": "Lorem ipsum dolor sit",
                "email": "lorem@ipsum.com"
            }
        }
        
class UserTokenSchema(BaseModel):
    id: int = Field(...)
    fullname: str = Field(...)
    email: EmailStr = Field(...)
    access_token: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "fullname": "Lorem ipsum dolor sit",
                "email": "lorem@ipsum.com",
                "access_token": "Bearer token"
            }
        }
        
class UserLoginSchema(BaseModel):
    email: EmailStr = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "lorem@ipsum.com",
                "password": "weakpassword"
            }
        }