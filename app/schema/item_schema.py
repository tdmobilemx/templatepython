from pydantic import BaseModel, Field

class ItemCreateSchema(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
    owner_id: int = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "title": "Lorem ipsum dolor sit",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "owner_id": 1
            }
        }
        
class ItemSchema(BaseModel):
    id: int = Field(...)
    title: str = Field(...)
    description: str = Field(...)
    owner_id: int = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Lorem ipsum dolor sit",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                "owner_id": 1
            }
        }