# Microservice FastApi Python

Template microservicio para uso de back de python

By ccrosas

## Ambientación

Se requiere:

- [Python 3.X](https://www.python.org/downloads/)
- [Visual Studio Code](https://code.visualstudio.com/Download)

### Plugins

 - [Python Essentials VS Marketplace](https://marketplace.visualstudio.com/items?itemName=JillMNolan.python-essentials)
 - [SQLite](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite)

## Ejecucion de Proyecto

Se recomienda crear un entorno virtual con Python

```
virtualenv desarrollo
```

Activamos el entorno virtual

```
.\desarrollo\Scripts\activate
```

para desactivarlo ejecutames

```
deactivate
```
### Instalar librerias

Para instalar las librerias se requerira ejecutar el siguiente comando.
```
pip install -r requirements.txt
```

Para obtener las librerias instaladas
```
pip list --format=freeze
```
## Ejecutar

Para ejecutar en modo debug se debera usar _F5_ en visual studio

Para ejecutar desde consola se usara el siguiente comando.
```
python main.py
```

