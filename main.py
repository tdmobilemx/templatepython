#pipreqs.exe --encoding utf8 --force
import multiprocessing
import uvicorn
import yaml

if __name__ == "__main__":
    #log_config = uvicorn.config.LOGGING_CONFIG
    #log_config["formatters"]["access"]["fmt"] = "%(asctime)s - %(levelname)s - %(message)s"
    #log_config = "conf/log.ini"
    log_config = yaml.safe_load(u"app/conf/log_conf.yaml")
    multiprocessing.freeze_support()
    uvicorn.run("app.api:app", host="0.0.0.0", port=8080, reload=False, workers=0, log_config=log_config)