FROM python_fastapi

LABEL maintainer="Carlos Rosas <ccrosas@compartamos.com>"

COPY ./ /app

COPY ./script.sh /

WORKDIR /

RUN chmod 755 script.sh

#RUN pip3 install --upgrade pip

RUN pip3 install --no-cache-dir -r /app/requirements.txt

ENTRYPOINT ["/script.sh"]
