import unittest
import json
from fastapi.testclient import TestClient
from app.api import app

client = TestClient(app)

class Test_Test(unittest.TestCase):
    assert True

def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Welcome"}


def test_register():
    headers = {
        'accept': 'application/json',
        'content-type': 'application/json'
    }
    data = {
        "fullname": "Lorem ipsum dolor sit",
        "email": "lorem3@ipsum.com",
        "password": "weakpassword"
    }
    #datainfo=json.load(data)
    response = client.post("/users", json=data, headers=headers)
    #assert response.status_code == 200
    assert response.json() == {"detail":"Email already registered"}


def test_UUID():
    headers = {
        'accept': 'application/json',
        'content-type': 'application/json'
    }
    #datainfo=json.load(data)
    response = client.get("/uuid", headers=headers)
    assert response.status_code == 200
    
def test_taskBackground():
    headers = {
        'accept': 'application/json',
        'content-type': 'application/json'
    }
    #datainfo=json.load(data)
    response = client.get("/taskBackground", headers=headers)
    assert response.status_code == 200